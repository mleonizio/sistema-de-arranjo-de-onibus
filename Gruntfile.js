module.exports = function(grunt) {
	'use strict';

  grunt.initConfig({

    uglify: {
      options: {
          banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
          sourceMap: true,
          sourceMapIncludeSources: true
      },
      dist: {
        files: {
          'assets/js/site.min.js': [
            'assets/js/jquery-1.10.1.min.js',
            'assets/js/bootstrap.min.js',
            'assets/bootflat/js/icheck.min.js',
            'assets/js/application.js'
          ]
        }
      }
    },

    cssmin: {
      options: {
          keepSpecialComments: 0,
          banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
      },
      dist: {
        files: {
          'assets/css/site.min.css': [
            'assets/css/bootstrap.min.css',
            'assets/bootflat/css/bootflat.css',
            'assets/css/site.css'
          ],
          'assets/bootflat/css/bootflat.min.css': 'assets/bootflat/css/bootflat.css'
        }
      }
    },

    sass: {
      dist: {
      	options: {
          style: 'expanded',
          sourcemap: 'true',
        },
        files: {
          'assets/bootflat/css/bootflat.css': 'assets/bootflat/scss/bootflat.scss'
        },
      }
    },

     watch: {

        files :["all"],
          options: {
              livereload:true,
            },

            controllers: {
            files: ['application/controllers/*.php', 'application/controllers/**/*.php'],
            
            },
        models: {
            files: ['application/models/*.php', 'application/models/*/*.php'],
            
            },
        views: {
            files: ['application/views/*/*.{php,html}', 'application/views/*.php'],
         },
   },
    pkg: grunt.file.readJSON('package.json')
  });

  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', [
    'sass',
    'cssmin',
    'watch',
    'uglify'
  ]);
};