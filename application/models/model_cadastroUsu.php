<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* CADASTRO DE USUARIOS TEMPORÁRIO
*/
class model_cadastroUsu extends CI_Model {

	private $admin_tabela = "ADMINISTRADOR";
	
	public function cadastroUsu()
	{
		
		$this->db->insert($this->admin_tabela);
		return true;
	}
}