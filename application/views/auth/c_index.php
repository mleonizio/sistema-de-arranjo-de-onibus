<?php

	/**
	*
	*
	* Sistema de Arranjo de ônibus Unificado
	* @author Marcelo Leonízio - eu@marceloleonizio.com
	*
	*
	**/

	echo doctype('html5');
	?>
<html lang="pt-br">
<head>
	<title>Sistema de Arranjo Unificado de Ônibus</title>
	<?php
	$meta = array(
        array('name' => 'robots', 'content' => 'no-cache'),
        array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv'),
        array('name' => 'X-UA-Compatible', 'content' => 'IE=edge', 'type' => 'equiv'),
        array('name' => 'viewport','content' => 'width=device-width, initial-scale=1'),
        array('name' => 'author','content' => 'Marcelo Leonizio - @mleonizio')
    );
	echo meta($meta);
	echo link_tag('http://fonts.googleapis.com/css?family=Roboto:400,300,700');
	echo link_tag('assets/css/formsigin.css');
	echo link_tag('assets/css/bootstrap.min.css');
   	echo link_tag('assets/css/site.min.css');
	?>
</head>
	<body>
