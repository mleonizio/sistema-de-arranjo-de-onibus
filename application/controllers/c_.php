<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class c_ extends CI_Controller {


	function __construct()
		{
			parent::__construct();

			$this->load->model('model_cadastroUsu');			

		}

	
	public function index()
	{

		$this->load->view('auth/c_index');
		$this->load->view('auth/index');
		$this->load->view('auth/f_index');
	}

	public function adminPainel()
	{
		$this->load->view('auth/c_index');
		$this->load->view('u_/index');
		$this->load->view('auth/f_index');
	}

	public function insereDadosUsuario()
	{	
		extract($_POST);
		$this->db->set('nome', $nomeCompleto);
		$this->db->set('nomeUsuario', $nomeUsuario);
		$this->db->set('congregacao', $nomeCongregacao);
		$this->db->set('telefone', $telefone);
		$this->db->set('password', $password);

		$this->model_cadastroUsu->cadastroUsu();

		$this->session->set_flashdata('cadastro-ok', 'Cadastro Realizado Com Sucesso');
										redirect('c_/index');


	}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */